unit splitTestCase;

interface
uses

    TestFramework
  , Classes
  , Windows
  , usplit
  ;

type

  TsplitTestCase = class(TTestCase)
  strict private
    res: StringA;
    c: Integer;

    tsl: TStringList;
  public
    procedure SetUp; override;
    procedure TearDown; override;

  published

    procedure EmptyTest;
    procedure OneElementTest;
    procedure TwoElementTest;
    procedure ThreeElementTest;
  end;


implementation

{ TsplitTextCase }
procedure TsplitTestCase.SetUp;
  begin
    tsl := TStringList.Create;
    res := nil;
    c := 0;
  end;

procedure TsplitTestCase.TearDown;
  begin
    tsl.Free;
    res := nil;
    c := 0;
  end;

procedure TsplitTestCase.EmptyTest;
begin
  split( '', '|', res, c );
  Check( c = 0 );

  split( '', '|', tsl );
  Check( tsl.Count = 0 );
end;

procedure TsplitTestCase.OneElementTest;
begin
  split( ' ', '|', res, c );
  Check( ( c = 1 ) and ( res[0] = ' ' ) );
  split( 'Hello World', '|', res, c );
  Check( ( c = 1 ) and ( res[0] = 'Hello World' ) );

  split( ' ', '|', tsl );
  Check( ( tsl.Count = 1 ) and ( tsl[0] = ' ' ) );
  tsl.Clear;
  split( 'Hello World', '|', tsl );
  Check( ( tsl.Count = 1 ) and ( tsl[0] = 'Hello World' ) );
end;

procedure TsplitTestCase.TwoElementTest;
begin
  split( ':', ':', res, c );
  Check( ( c = 2 ) and ( res[0] = '' ) and ( res[1] = '' ) );
  split( ':Hello', ':', res, c );
  Check( ( c = 2 ) and ( res[0] = '' ) and ( res[1] = 'Hello' ) );
  split( 'Hello:', ':', res, c );
  Check( ( c = 2 ) and ( res[0] = 'Hello' ) and ( res[1] = '' ) );

  split( ':', ':', tsl );
  Check( ( tsl.Count = 2 ) and ( tsl[0] = '' ) and ( tsl[1] = '' ) );
  tsl.Clear;
  split( ':Hello', ':', tsl );
  Check( ( tsl.Count = 2 ) and ( tsl[0] = '' ) and ( tsl[1] = 'Hello' ) );
  tsl.Clear;
  split( 'Hello:', ':', tsl );
  Check( ( tsl.Count = 2 ) and ( tsl[0] = 'Hello' ) and ( tsl[1] = '' ) );
end;

procedure TsplitTestCase.ThreeElementTest;
begin
  split( '::', ':', res, c );
  Check( ( c = 3 ) and ( res[0] = '' ) and ( res[1] = '' ) and ( res[2] = '' ) );
  split( 'a::', ':', res, c );
  Check( ( c = 3 ) and ( res[0] = 'a' ) and ( res[1] = '' ) and ( res[2] = '' ) );
  split( ':a:', ':', res, c );
  Check( ( c = 3 ) and ( res[0] = '' ) and ( res[1] = 'a' ) and ( res[2] = '' ) );
  split( '::a', ':', res, c );
  Check( ( c = 3 ) and ( res[0] = '' ) and ( res[1] = '' ) and ( res[2] = 'a' ) );

  split( '::', ':', tsl );
  Check( ( tsl.Count = 3 ) and ( tsl[0] = '' ) and ( tsl[1] = '' ) and ( tsl[2] = '' ) );
  tsl.Clear;
  split( 'a::', ':', tsl );
  Check( ( tsl.Count = 3 ) and ( tsl[0] = 'a' ) and ( tsl[1] = '' ) and ( tsl[2] = '' ) );
  tsl.Clear;
  split( ':a:', ':', tsl );
  Check( ( tsl.Count = 3 ) and ( tsl[0] = '' ) and ( tsl[1] = 'a' ) and ( tsl[2] = '' ) );
  tsl.Clear;
  split( '::a', ':', tsl );
  Check( ( tsl.Count = 3 ) and ( tsl[0] = '' ) and ( tsl[1] = '' ) and ( tsl[2] = 'a' ) );
end;

initialization
  RegisterTest(TsplitTestCase.Suite);

end.
