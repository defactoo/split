program split;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  Windows,
  Classes,
  usplit in 'usplit.pas';

const
  cCount = 1000000;
var
  i: Integer;
  tsl: TStringList;

  fFrequency,
  fstart,
  fStop : Int64;

begin
  if ParamCount >= 2 then
  begin
    tsl := TStringList.Create;
    try
      usplit.split( ParamStr( 1 ), ParamStr( 2 ), tsl );
      for i := 0 to tsl.Count - 1 do
        Writeln( tsl[i] );
    finally
      tsl.Free;
    end;
  end else
  begin
    Writeln( 'split.exe input delimiter' );
  end;
{  s := 'LOG|org.example|application|filter|2012-06-02T09:36:45|message';

  QueryPerformanceFrequency( fFrequency );
  QueryPerformanceCounter( fStart );

  for i := 0 to cCount - 1 do
    usplit.split( s,'|', stra, count );

  QueryPerformanceCounter( fStop );
  Writeln( Format( 'Total elapsed time for splitstring: "%s"  %d times : %f msec',
    [ s, cCount, ((MSecsPerSec * (fStop - fStart)) / fFrequency) ]) );
}
end.
