unit usplit;

interface

uses
  Classes
  ;
  
type
  StringA = Array of Utf8String;

function Split( str: Utf8String; delimiter: Utf8String; var res: StringA; var count: Integer ): Boolean; overload;
function Split( str: Utf8String; delimiter: Utf8String; res: TStrings ): Boolean; overload;

implementation

uses
    StrUtils
  ;

function Split( str: Utf8String; delimiter: Utf8String; var res: StringA; var count: Integer ): Boolean;
const
  cIncA = 10;

var
  i, l,
  offs: Integer;

begin
  count := 0;
  offs := 1;
  i := PosEx( delimiter, str, offs );
  l := Length( res );
  while i > 0 do
  begin
    if count = l then
      SetLength( res, l + cIncA );
    res[count] := Copy( str, offs, i - offs );
    offs := i + 1;
    inc( count );
    i := PosEx( delimiter, str, offs );
  end;

  if Length( str ) > 0  then
  begin
    if count = l then
      SetLength( res, l + cIncA );
    res[count] := Copy( str, offs, Length( str ) - offs + 1 );
    inc( count );
  end;

  result := count > 0;
end;

function Split( str: Utf8String; delimiter: Utf8String; res: TStrings ): Boolean;
var
  i,
  offs: Integer;
begin
  offs := 1;
  i := PosEx( delimiter, str, offs );
  while i > 0 do
  begin
    res.Add( Copy( str, offs, i - offs ) );
    offs := i + 1;
    i := PosEx( delimiter, str, offs );
  end;
  if Length( str ) > 0  then
    res.Add( Copy( str, offs, Length( str ) - offs + 1 ) );
  result := res.count > 0;
end;

end.
